import React from 'react'
import {Route,Switch} from 'react-router-dom'
import PropTypes from 'prop-types'

export const PrivateRouter =({
    isAuthenticated,
    component:Component,
    ...rest
}) => {

    localStorage.setItem('lastPath',rest.location.pathname)

    return (
    <>
        <Switch>
        <Route {...rest} 
            component = {(props) => ((<Component {...props}/>))}
        />
        </Switch>
    </>
    )
}

PrivateRouter.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    component: PropTypes.func.isRequired
}