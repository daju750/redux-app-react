import React from "react";
import {Route, Switch} from 'react-router-dom'
import {NavBar} from "../ui/NavBar";
import {Home} from "../Screen/Home";

export const DashboardRouters = () => {
    
    return (
    <>
        <NavBar/>
        <div className="container mt-3">
            <Switch>
                <Route path="/" component={Home}/>
            </Switch>
        </div>
    </>
    )
}