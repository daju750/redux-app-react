import React from "react";
import {BrowserRouter as Routers,Route,Switch} from 'react-router-dom'
import {LoginScreen} from "../Screen/Login";
import {DashboardRouters} from "../routers/DashboardRouters";
import {PrivateRouter} from "../routers/PrivateRouter";

export const AppRouter = () => {
    //const {user} = useContext(AuthConext)
    return (
        <>
        <Routers>
            <Switch>                
                <Route exact path="/Login" component={LoginScreen}/>
                <PrivateRouter path="/" component={DashboardRouters}/>
            </Switch>
        </Routers>
        </>
    )
}